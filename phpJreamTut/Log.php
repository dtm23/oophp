<?php

class Log
{

	private $_strFileName;
	private $_strData;

	/**


	**/
	public function write($strFileName,$strData){

		//set class var
		$this->_strFileName = $strFileName;
		$this->_strData = $strData;
		
		//check data
		$this->_checkPermission();
		$this->_checkStrData();

		//write the file
		$handle = fopen($strFileName, 'a+'); //store int handle
		//create and pass data in second parameter
		//add a string regex to put new line
		fwrite($handle, $strData."\r\n"); 

		fclose($handle); //close the file
	}

	/**
	* @desc reading a given file
	* @param str $strFileName the name fo the file
	* @return str

	**/
	public function read($strFileName)
	{

		$this->_strFileName = $strFileName;

		$this->_checkFileExits();

		$handle = fopen($strFileName, "r"); // read the file

		return file_get_contents($strFileName);

	}

	private function _checkFileExits(){
		if(!file_exists($this->_strFileName)){
			die("File not fould : ".$this->_strFileName);
		}
	}

	private function _checkStrData()
	{
		if(strlen($this->_strData)<1){
			die("Empty file: ".$this->_srtData);
		}
	}

	private function _checkPermission()
	{
		//check if the file is writeable
		if(!is_writable($this->_strFileName))
			die('Change your chmod permission to '.$this->_strFileName);
	}
}