<?php

	include('dbconnect.php');

	/*
		Class Objects 
		we will fetch data from database and assign to a class properties.
		they will automatically assign on similar name.
		we can also add extra variables and define it.
	*/

	// Normally you should have a class on it's own page.
	class StudentBookEntry{
		//variables
		public $name,$address,$class,
		$state; //new var to show student name and class 

		//when we create instance, we define state 
		public function __construct(){
			$this->state = "Name : ".$this->name." Class ".$this->class;
		}
	}


	$q = $handler->query("SELECT * from students");
	echo "<pre>";

	// $r = $query->fetch(PDO::FETCH_OBJ); //fetch number (index) only

	// print_r($r);
	// while($data = $query->fetch()){
	// 	echo $data['name'], "<br>";
	// }


	$q->setFetchMode(PDO::FETCH_CLASS,'StudentBookEntry'); //as Class objects,from given class

	// $r=$query->fetchAll();  we can use fetchAll() to print all the data from db

	// //input data, let's say from post from form
	// $name = "kaka";
	// $address = "Brazil";
	// $class = "World class";

	// $sql = "INSERT INTO `students`(`name`, `address`, `class`) VALUES (:name,:address,:class)";

	// $query = $handler->prepare($sql); //don't run but make ready for execute

	// $query->execute(array(
	// 	':name' => $name,
	// 	':address' => $address,
	// 	':class' => $class
	// ));

	$q = $handler->query("SELECT * from students");

		//geting row count
	if($q->rowCount()){
		while($r= $q->fetch(PDO::FETCH_ASSOC)){
			echo $r->name, "<br>";
		}
	}else{
		echo "No results";
	}


	echo "</pre>";
	
?>