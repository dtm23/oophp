<?php
  include('includes/head.php');
?>
    <div class="content_wrapper">
      
      <div class="SideBar">
        <div id="sidebar_title">Categories</div>
        <div id="category">
          <ul>

            <?php
             
              $query_cat = "select * from categories";

              $result = mysqli_query($con,$query_cat);
              while($data = mysqli_fetch_assoc($result))
              {
                  $cat_id = $data['cat_id'];
                  $cat_title = $data['cat_title'];

                  echo " <li><a href='index.php?catid=$cat_id'>$cat_title</a></li>";
              }
             ?>
          </ul>
        </div>
        <div id="sidebar_title">Brands</div>
        <div id="category">
          <ul>

            <?php
              $query_cat = "select * from brand";

              $result = mysqli_query($con,$query_cat);

              while($data = mysqli_fetch_assoc($result))
              {
                  $brand_id = $data['brand_id'];
                  $brand_title = $data['brand_title'];

                  echo " <li><a href='index.php?brandid=$brand_id'>$brand_title</a></li>";

              }

             ?>    
          </ul>
        </div>
      </div>
    
      <div class="MainSection">
        <div class="offers">
          here all the offeres or promotion, update will be to publish - headlines
        </div>
        <div class="carasoul">

            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
              <!-- indicate here ! to move next or previous -->
              <div class="carousel-inner">
                <div class="carousel-item active">
                  <img class="d-block w-100" src="img/carosoul.jpg" alt="First slide">
                </div>
                <div class="carousel-item">
                  <img class="d-block w-100" src="img/carosoul2.jpg" alt="Second slide">
                </div>
          
              </div>
              <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
            </div>
         
        </div>
        <?php
          include('includes/footer.php');
        ?>

        
      </div>
    </div>


  <?php
    include('includes/jscontent.php');
  ?>

</body>
</html>
