<?php
	include('../includes/dbconnection.php');
?>
<!DOCTYPE html>
<html>
<head>
	<title>
		Insert product
	</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
	<script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
 	<script>tinymce.init({ selector:'textarea' });</script>
</head>
<body>
	<div class="container">
		<h2 align="center" style="padding: 30px;">Add Prodcut to the List</h2>

		<div style="padding: 100px;">
			<form method="post" action="insert_product.php" enctype="multipart/form-data">
				<h2> Update here !</h2>
			  <div class="form-group">
			    <label for="product_title">New - Product title</label>
			    <input type="text" name="product_title" class="form-control" id="product_title" >
			  </div>
			  <div class="form-group">
			  	<div class="input-group mb-3">
				  <div class="input-group-prepend">
				    <label class="input-group-text" for="product_cat">Product Category</label>
				  </div>
				  <select class="custom-select" id="product_cat" name="product_cat">
				  	<option selected> Choose... </option>
				  	<?php
				  		$query = "SELECT * from categories";

		             	$result = mysqli_query($con,$query);

		              	while($data = mysqli_fetch_assoc($result))
		              	{
		                  $cat_id = $data['cat_id'];
		                  $cat_title = $data['cat_title'];
		                  echo "<option value='$cat_id'>$cat_title</option>";

		              	}
				  	?>
				  </select>
				</div>

			  </div>

			  <div class="form-group">
			  	<div class="input-group mb-3">
				  <div class="input-group-prepend">
				    <label class="input-group-text" for="product_brand">Brand</label>
				  </div>
				  <select class="custom-select" id="product_brand" name="product_brand">
				  	<option selected> Choose... </option>
				  	<?php
				  		$query = "SELECT * from brand";

		             	$result = mysqli_query($con,$query);

		              	while($data = mysqli_fetch_assoc($result))
		              	{
		                  $brand_id = $data['brand_id'];
		                  $brand_title = $data['brand_title'];
		                  echo "<option value='$brand_id'>$brand_title</option>";

		              	}
				  	?>
				  </select>
				</div>

			  </div>
			
			  
			  <div class="form-group">
			    <label for="img1">Product Image 1</label>
			    <input type="file" name="img1" class="form-control" id="img1" >
			  </div>  
			  <div class="form-group">
			    <label for="img2">Product Image 2</label>
			    <input type="file" name="img2" class="form-control" id="img2" >
			  </div>  
			  <div class="form-group">
			    <label for="img3">Product Image 3</label>
			    <input type="file" name="img3" class="form-control" id="img3" >
			  </div>  
			   <div class="form-group">
			    <label for="product_price">Product Price</label>
			    <input type="text" name="product_price" class="form-control" id="product_price" >
			  </div>
			   <div class="form-group">
			    <label for="product_desc">Product Description</label>
			    <input type="textarea" name="product_desc" class="form-control" id="product_desc" >
			  </div>
			   <div class="form-group">
			    <label for="product_status">Product Status</label>
			    <input type="text" name="product_status" class="form-control" id="product_status" >
			  </div>
			  <button type="submit" name="submit" class="btn btn-warning btn-lg btn-block">Add Product</button>
			</form>
		</div>

	</div>

</body>
</html>

<?php
	
	if(isset($_POST['submit'])){

		$product_title = $_POST['product_title'];
		$product_cat = $_POST['product_cat'];
		$product_brand = $_POST['product_brand'];
		$product_img_1 = $_FILES['img1']['name'];
		$product_img_2 = $_FILES['img2']['name'];
		$product_img_3 = $_FILES['img3']['name'];
		$product_price = $_POST['product_price'];
		$product_desc = $_POST['product_desc'];
		$product_status = $_POST['product_status'];

		//temp image name
		$temp_img1 = $_FILES['img1']['tmp_name'];
		$temp_img2 = $_FILES['img2']['tmp_name'];
		$temp_img3 = $_FILES['img3']['tmp_name'];

		move_uploaded_file($temp_img1,"product_images/$product_img_1");
		move_uploaded_file($temp_img2,"product_images/$product_img_2");
		move_uploaded_file($temp_img3,"product_images/$product_img_3");

		// write a validation of input
		// or you can do it on html5 or bootstrap required function
		$query = "INSERT INTO `products` (`cat_id`, `brand_id`, `product_title`, `product_img1`, `product_img2`, `product_img3`, `product_price`, `product_desc`, `product_status`) VALUES ('$product_cat', '$product_brand', '$product_title', '$product_img_1', '$product_img_2', '$product_img_3', '$product_price', '$product_desc', '$product_status');";

		$run_query = mysqli_query($con,$query);

		if($run_query){
			echo "<script> alert('Product addes successfully!');</script>";
		}
	}
	
?>