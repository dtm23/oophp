<?php
  include('includes/dbconnection.php');
?>

<!DOCTYPE html>
<html>
<head>
  <title>
    Online Shopping
  </title>
  <link rel="stylesheet" type="text/css" href="css/styleindex.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">

</head>
<body>
  
  <div class="main_wrapper">
    <div class="mynavbar" style="margin:auto;top: 0;margin-top: 10px;">
      <ul id="menu">
        <li><a href=""><i class="fas fa-venus"></i> Women</a></li>
        <li><a href=""><i class="fas fa-mars"></i> Men</a></li>
      </ul>
    </div>
    
    <!-- Header -->
    <div class="contenent" style="top: 0;right: 0;">
      <div class="row" style="float: right;">
        <div class="col-12">
          <table class="table" >
            <tr>
              <td style="border-right: rgb(23,32,32,32);">Customer Service</td>  
              <td> | <img src="img/gb.png" height="15" height="15"></td>
            </tr>
          </table>
        </div>
      </div>
    </div>
    <div class="header_wrapper" align="center" style="margin-bottom: 40px;">
          <img src="img/header_logo.jpg" style="margin-left:310px;margin-top:30px;height: 60px;background-color: white;">
    </div>
    <div style="float: left; margin-top: -35px;margin-left: 10px;">
        <form action="search.php" method="post" style="">
          <input type="text" name="search_query" placeholder="search" style="width: 350px; height: 30px;margin-top: -30px;">
          <button name="search"><i class="fas fa-search"></i></button>
        </form>
    </div>
    <div class="" style="float: right;margin-top: -21px; font-size: 14px;padding-right: 12px;">
      <ul id="signuplogin">
        <li><i class="fas fa-user-circle"></i> My Account</li>
        <li><i class="fas fa-heart"></i> Wish List</li>
        <li><i class="fas fa-user-plus"></i> Sign Up</li>
        <li><i class="fas fa-shopping-basket"></i> Basket</li>
      </ul>
    </div>
    <hr>
    
    <div class="info">
      <table>
        <tr>
          <th style="width:5%"></th>
          <th style="width:30%" align="center">GET YOUR'S 15% STUDENT DISCOUNTS with student's bean</th>
          <th style="width:30%" align="center">MORE THEN 70% ON SALES</th>
          <th style="width:30%;" align="center">FREE UK DELIVERY OVER £60 ORDERSS</th>
          <th style="width:5%"></th>
        </tr>
        
      </table>
    </div>
  </div>

<!-- </body>
</html>  -->