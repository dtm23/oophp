<?php


?>
<!DOCTYPE html>
<html>
<head>
	<title>
		use of include!
	</title>
</head>
<body>
	<div class="top">
		<p>Use of includes in php</p>
	</div>

	<div class="center">
		<div class="centertop">
			<ul>
				<li>include(file location here)</li>
				<li>require(file location here</li>
				<li>require_once(file location here)</li>
				<li>include_once(file location here)</li>
			</ul>
		</div>
	</div>

</body>
</html>

