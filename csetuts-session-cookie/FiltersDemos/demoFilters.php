<!DOCTYPE html>
<html>
<head>
	<title>
		PHP Filters...
	</title>
	
	<style type="text/css">
		.filter{
			padding-top: 0px;
			left: 0;
			margin: auto;
		}
	</style>
</head>
<body>
	<div class="filter">
		<div class="note">
			<p>
				<strong>Sanitizing data: </strong> Remove any illegal character from the data.

			</p>
		</div>

	</div>

	<pre>
		<?php
		$emails = array(
		 
		    'test@localhost',
		    'test@localhost.com'
		);

		foreach ($emails as $email) {
		    echo (filter_var($email, FILTER_VALIDATE_EMAIL)) ? 
		        "[+] Email '$email' is valid\n" : 
		        "[-] Email '$email' is NOT valid\n";
		}

	?>
		
	</pre>
	
</body>
</html>