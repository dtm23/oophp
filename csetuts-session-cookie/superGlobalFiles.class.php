
<!DOCTYPE html>
<html>
<head>
	<title>Using superglobal File</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
</head>
<body>
	<div class="container">
		
		<h2>USING $_FILES SUPER GLOBALS </h2>
		<p>Using and Testing a $_FILES superglobals</p>

		<form action="	fileupload.class.php" method="post" enctype="multipart/form-data">
			<table>
				<tr>
					<th colspan="2">Upload your file here! </th>
				</tr>
				<tr>
					<td>$_FILES example</td>
					<td>
						<input type="file" name="usersfile" value="Upload">
					</td>
				</tr>
				<td colspan="2">
					<input type="submit" name="">
				</td>
			</table>
		</form>
	</div>
</body>
</html>