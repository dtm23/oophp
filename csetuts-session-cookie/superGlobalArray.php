<?php
	function showBrowers()
	{
		echo $_SERVER['PHP_SELF'];
	}


?>

<!DOCTYPE html>
<html>
<head>
	<title>Super globals</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">

</head>
<body>


	<div class="container">
		<div class="title" ">
			<h2>Here! we shall look at the super globals with some examples:</h2>
			<h4>List of Super globals</h4>
			<ul >
				<li>$GLOBALS</li>
				<li>$_SERVER</li>
				<li>$_GET</li>
				<li>$_POST</li>
				<li>$_REQUEST</li>
				<li>$_FILES</li>
				<li>$_COOKIE</li>
				<li>$_SESSION</li>
			</ul>
		</div>
		<h4 style="padding-top: 50px;">$_SERVER
		</h4>
		<div class="superglobal" style="padding-top: 30px;">


		<table class="table" >
			<thead>
				<th scope="col" class="font-weight-bold">Elements</th>
				<th scope="col" class="font-weight-bold">Description</th>
			</thead>
			<tbody>
				<tr>
					<th scope="row">$_SERVER['HTTP_HOST']</th>
					<td>To known host name</td>
				</tr>
				<tr>
					<th scope="row">$_SERVER['HTTP_USER_AGENT']</th>
					<td>
						To know browser's details of users
					</td>
				</tr>
				<tr>
					<th scope="row">$_SERVER['SERVER_NAME']</th>
					<td>Return the name of the host servere
					</td>
				</tr>
				<tr>
					<th scope="row">$_SERVER['REMOTE_ADDR']</th>
					<td>Return the IP address from where the users is viewing the current page.</td>
				</tr>

			</tbody>
		</table>

		<div class="" style="">
			<?php
				echo "HTTP_HOST : ".$_SERVER['HTTP_HOST']."<br>";
				echo "HTTP_USER_AGENT : ".$_SERVER['HTTP_USER_AGENT']."<br>";
				echo "REMOTE_ADDR : ".$_SERVER['REMOTE_ADDR']."<br>";
				echo "::1 ---> 127.0.0.1 (local server)"."<br>";
				echo "SERVER_PORT : ".$_SERVER['SERVER_PORT']."<br>";
				echo "PHP_SELF : ".$_SERVER['PHP_SELF']."<br>";

			?>
		</div>
		<div class="withfunction" style="background-color: gray; margin-top: 40px;padding: 10px;padding-bottom: 30px;">
			<h5>$_SERVER use on funtions examples </h5>
			<hr>
			<p>File location : <?php showBrowers() ?></p>
		</div>

		<div class="get" style="padding-top: 30px;">
			<!-- 

			There are two ways through which cient (browser can send information to the web server) 

				1) $_GET
				2) $_POST
			-->
			<h2>Example of GET method</h2>
			<form class="form" action="superGlobalArray.class.php" method="get">

				<div class="row">
					<div class="col-6">
						Name
					</div>
					<div class="col-6">
						Email
					</div>
				</div>
				<div class="row">
					<div class="col-6">
						<input class="form-control" type="text" name="name" placeholder="">
					</div>
					<div class="col-6">
						<input class="form-control" type="text" name="email" placeholder="">
					</div>
					<div class="col-6">
						<label>Gender : </label>
						Male: <input type="radio" name="gender" value="Male">
						Female: <input type="radio" name="gender" value="Female">
					</div>
					<div class="col-6">
						<input class="float-right" type="submit" value="Submit" >
					</div>
				</div>
			</form>

			<h2 style="padding-top: 20px;">Example of Post method</h2>
			<form class="form" action="superGlobalArray.class.php" method="post">

				<div class="row">
					<div class="col-6">
						Name
					</div>
					<div class="col-6">
						Email
					</div>
					
				</div>
				<div class="row">
					<div class="col-6">
						<input class="form-control" type="text" name="name" placeholder="">
					</div>
					<div class="col-6">
						<input class="form-control" type="text" name="email" placeholder="">
					</div>
					<div class="col-6">
						<label>Password</label>
						<input class="form-control" type="password" name="password" placeholder="">
					</div>
					<div class="col-6" style="padding-top: 40px;">
						<label>Gender : </label>
						Male: <input type="radio" name="gender" value="Male">
						Female: <input type="radio" name="gender" value="Female">
					</div>

					<div class="col-6">
						<input class="float-right btn-success" type="submit" value="Submit" >
					</div>
				</div>
			</form>
			
		</div>

		</div>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>sd
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
	</div>

</body>
</html>