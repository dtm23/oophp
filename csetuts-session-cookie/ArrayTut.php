<?php


	$array = array();
	
	$array[0] = 1;
	$array[1] = 2;
	$array[2] = 3;
	$array[] = 4;

	//sub array

	$arr[0] = array(23,33,32,23);

	/*
		$arr[0] = array(23,23,33,32,23);
		is same as 
		$arr[0][23]
		$arr[0][33]
		$arr[0][32]
		$arr[0][23]
	*/

		//for loop
		for($i = 0;$i<=3;$i++){
			echo $array[$i]."<br>";
		}
		echo "<hr>";

		//while loop
		$i = 0;
		while($i<=3){

			echo $array[$i]."<br>";
			$i++;
		}
		echo "<hr>";
		//do while loop
		$i = 0;
		do{
			echo $array[$i].",";
			$i++;
		}while($i>=4);

		//for each loop with associative array
		echo  "<hr>";
		$arr1 = array("Name"=>"Devendra","Love"=>"Kamala",23,32,3);
		$arr2 = array("Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday");

		foreach ($arr1 as $key=>$value) {
			echo $key." => ".$value."<br>";
		}

		echo  "<hr>";
		for($i=0;$i<count($arr2);$i++){
			echo $arr2[$i]."<br>";
		}






