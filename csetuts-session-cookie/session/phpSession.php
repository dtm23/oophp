<?php
	$nameErr = $passwordErr = "";

session_start();

	if(isset($_SESSION['uName'])){
		header('location:welcome.php');
		exit();
	}

		if(isset($_POST['login'])){
		if($_SERVER['REQUEST_METHOD'] == 'POST')
		{
			if(empty($_POST['username']) && empty($_POST['password'])){
				$nameErr = "Name is required";
				$passwordErr ="Password must not be empty";

			}
			elseif(empty($_POST['username']))
			{
				$nameErr = "Name is required";
			}elseif (empty($_POST['password']))
			{
				$passwordErr ="Password must not be empty";
			}else
			{
				$data = testInput($_POST['username']);
				$_SESSION['uName'] = $data ;
				header('location:welcome.php');
			}

		}

	}
	function testInput($data){
		$data = trim($data);
		$data = stripcslashes($data);
		$data = htmlspecialchars($data);
		return $data;
	}
?>

<!DOCTYPE html>
<html>
<head>
	<title>
		php Session
	</title>

	<style type="text/css">
		.error {color: #FF0000;}
	</style>
	
</head>
<body>

	<div class="container">
		<div class="myfrom">
			<form method="POST" action="<?php echo $_SERVER['PHP_SELF'] ?>">
				<table align="center" style="padding-top: 80px;">
					<tr>
						<td>UserName</td>
						<td>
							<input type="text" name="username">
							<span class="error"><?php echo $nameErr ?></span>
						</td>
						
					</tr>
					<tr>
						<td>Password</td>
						<td>
							<input type="password" name="password">
							<span class="error"><?php echo $passwordErr ?></span>
						</td>
						
					</tr>
					<tr>
						<td colspan="2" align="center">
							<input  type="submit" name="login">
						</td>
					</tr>
				</table>
				
			</form>
		</div>
	</div>

</body>
</html>

