<?php 
	//two different method to look at it
	


	// method one explain

	$array = [
		'name' => [
			'your name is required',
			'your name cannot contain any numbers'
		],
		'dob' => [
			'your date of birth is required',
		],
		'password' => [
			'your password must be 6 character or more',
			'your password is\'t strong enough'
		]
		,
		'Something just a text line'
	];

	//define new function flattern_array
	function flatten_array(array $items, array $flatten = []){
		foreach ($items as $item) {
			if(is_array($item)){
				$flatten = flatten_array($item, $flatten);
				continue;
			}
			$flatten[] = $item;
		}

		return $flatten;
	}
	echo "<pre>"; 
		// var_dump(flatten_array($array));
	echo "</pre>"; 


	// method two explain

	$arr = new RecursiveArrayIterator($array);

	$arr = new RecursiveIteratorIterator($arr);

	$arr = iterator_to_array($arr, false);

	//or another or way do it...
	$flatten = iterator_to_array(
		new RecursiveIteratorIterator(
			new RecursiveArrayIterator($array)
		),
	false);

	foreach ($flatten as $f) {
		echo $f."<br>";
	}




	// echo "<pre>"; 
	// 	var_dump($flatten);
	// echo "</pre>"; 

?>