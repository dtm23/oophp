<?php

include("app/dbconnection.php");

class StudentBookEntry{
	public $name,$email,$address;

	public function __construct(){

	}

	public function getDetails(){
		echo "Name : ".$this->name;
		echo "Address : ".$this->address;
		echo "Email : ".$this->email;
	}


}



if(isset($_POST['submit'])){

	$email = $_POST['email'];

	// anyone can run a SELECT * from stuents where email = '$email'" sql query to modify or delete any db table
	// so we need to use a prepare method to avoid this

	$user = $con->prepare("SELECT * from students where email = :email"); // adding a placeholder for email
	// later when executed we can pass email address from there

	//we are going to  
	$user->execute([
		'email' => $email,
	]);


	if($user->rowCount()){
		$user->setFetchMode(PDO::FETCH_CLASS,'StudentBookEntry'); //as Class objects,from 

		$student = $user->fetch();

		echo "Name : ".$student->name."<br>";
		echo "Email : ".$student->email."<br>";
		echo "Address : ".$student->address."<br>";

		echo "<br>";
		echo "<br>";
		echo "<br>";
	}else{
		echo "No matched found! :(";
		echo "<br>";
		echo "<br>";
		echo "<br>";
	}


	
	


}
	


?>

<!DOCTYPE html>
<html>
<head>
	<title>
		SQL Injection!
	</title>
</head>
<body>
	<form action="SQLinjection.php" method="post" >
		<label for="email">Email Address
			<input type="text"	name="email" id="email" >
		</label>
		
		<input type="submit" name="submit" value="Submit">
	</form>

</body>
</html>