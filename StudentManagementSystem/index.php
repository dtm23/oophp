<?php 
// Turn off all error reporting
// error_reporting(0);
include('head.php');
include('function.php');
?>
    <nav class="navbar navbar-light bg-light">
      <a class="navbar-brand" href="index.php">
        <img src="images/dtmlog.png" width="" height="" alt="">
      </a>
      <ul class="navbar-nav">
        <?php 
        if(!isset($_SESSION['uid'])){
          echo '<li class="nav-item">
          <a href="login.php">Login</a> | <a href="register.php">Register</a>
          </li> ';
        }

        ?>
        
       
      </ul>
    </nav>

    <div class="container">
      <form action="index.php" method="post">
        <h1 align="center">
          Student Management System (SMS)
        </h1>
        <hr>

        <table align="center" style="width:50%;">
          <tr>
            <td colspan="2" align="center">
              <h2>Student Information</h2>
            </td>
          </tr>
          <tr>
            <td>Student Standerds</td>
            <td>
              <select name="std">
                <option value="1">One</option>
                <option value="2">Two</option>
                <option value="3">Three</option>
                <option value="4">Four</option>
                <option value="5">Five</option>
                <option value="6">Six</option>
                <option value="7">Seven</option>
                
              </select>
            </td>
          </tr>
          <tr>
            <td>Enter Roll number</td>
            <td><input type="number" name="rollno"></td>
          </tr>
          <tr>
            <td align="center" colspan="2">
              <button type="submit" name="submit" class="btn btn-success" style="margin-top: 10px;">Show Info!</button> 
            </td>
          </tr>
        </table>
      </form>

    <div class="result" style="margin-top: 30px;">
       <?php  
          if(isset($_POST['submit']))
          {
            $std = $_POST['std'];
            $rollno = $_POST['rollno'];

            showStudentDetails($std,$rollno);
          }
        ?>

    </div>

  
   
  </body>
</html>