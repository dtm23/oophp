<?php
session_start();
unset($_SESSION['uid']);
//you can use a session_destroy() to unset all variables
//unset can only unset given session variables.
header('location:index.php');