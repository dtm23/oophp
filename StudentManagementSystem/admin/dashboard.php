<?php
  session_start();
  if(!isset($_SESSION['uid'])){
    header('location:../index.php');
  }else{
    //
  }
?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">



    <title>SMS</title>

    <style type="text/css">
      .images{
        max-width: 150px;
        max-width: 150px;
      }
      .img-container{
        margin:auto;
        top: 0;
        margin-top: 60px;
      }
      a:link    {
        /* Applies to all unvisited links */
        text-decoration:  none;
        font-weight:      bold;
      } 
      .logout{
        font-size: 15px;
        font-family: cursive;
        color: black;
      }


    </style>
  </head>
  <body>
     <nav class="navbar navbar-light bg-light">
      <a class="navbar-brand" href="../index.php">
        <img src="../images/dtmlog.png" width="" height="" alt="">
      </a>
      <ul class="navbar-nav">
        <li class="nav-item">
          <i class="fas fa-sign-out-alt"> <a class="logout" href="../logout.php"> Logout</a></i>


        </li> 
       
      </ul>
    </nav>

    <div class="container">

      <div class="row">
        <div class="col-8" style="">
            <div class="row " style="background-color: #DFDFDF; margin-top: 50px;padding: 50px; " >
              <div align="center" class="col-sm">
                <a href="addStudent.php">
                  <img class="images img-container" src="../images/add.jpg">
                  <br>
                  <h5 class="badge badge-info text-uppercase" style="margin:auto;top:0;margin-top: 6px;">Add Student</h5>
                </a> 
              </div>
            <div align="center" class="col-sm img-container">
              <a href="deleteStudent.php">
                <img class="images" src="../images/del.jpg">
                <br>
                <h5 class="badge badge-info text-uppercase">Delete Student</h5>
              </a>
              
            </div>
            <div align="center" class="col-sm img-container">
              <a href="updateStudent.php">
                <img class="images" src="../images/update.png">
                <br>
                <h5 class="badge badge-info text-uppercase">Update Student</h5>
              </a>
            </div>
          </div>
        </div>
        <div class="col-4"style="margin-top: 55px;">
          <?php  
            include('../dbconnection.php');

            $query = "SELECT * FROM `students`";
            $run = mysqli_query($con,$query);
            if(mysqli_fetch_assoc($run) <1)
            {
              //
            }else
            {
              ?>
              <table class="table" >
                <thead>
                  <tr>
                    <th>S/N</th>
                    <th>Full Name</th>
                    <th>Image</th>
                  </tr>
              </thead>
              <tbody>
              <?php
              $count = 0;
              while ($data = mysqli_fetch_assoc($run)) {
                  $count++;
                  $full_name = $data['first_name'].' '.$data['last_name'];
                  ?>
                  
                   <tr>
                    <th scope="row"><?php echo $count; ?></th>
                    <td> <a href="updateform.php?sid=<?php echo $data['id'] ?>"><?php echo $full_name; ?></a></td>
                    <td><img src="../dataimg/<?php echo $data['image'] ?>" widht="50" height="50"/></td>
                  </tr>
                   <?php
              }?>   
              <?php
            }
            ?>
        </div>
      </div>
  
    </div>
  </body>
</html>
