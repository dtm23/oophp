<?php
  include('adminHead.php');

  $sid = $_GET['sid']; //store id, that we passed it from updateStudent.php

  $query = "SELECT * FROM `students` WHERE `id` = '$sid'"; //find any data from database that matches the id
  $run = mysqli_query($con,$query); //run sql with db connection, $con is connection and sql query 

  $data = mysqli_fetch_assoc($run); //data hold all the results from fetch in an array form

  if(isset($_POST['submit']))
  {
    $first_name = $_POST['first_name'];
    $last_name = $_POST['last_name'];
    $roll_no = $_POST['roll_number'];
    $email = $_POST['email'];
    $standard = $_POST['standard'];
    $id =$_POST['sid'];
    $image = $_FILES['simage']['name'];
    $temp_image_location = $_FILES['simage']['tmp_name'];

    move_uploaded_file($temp_image_location, "../dataimg/$image");

    $q = "UPDATE `students` SET `first_name` = '$first_name', `last_name` = '$last_name', `roll_number` = '$roll_no', `email` = '$email', `standerd` = '$standard', `image` = '$image' WHERE `id` = '$id'";

    $row = mysqli_query($con,$q);

    if($row == true)
    {
      ?>
        <script type="text/javascript">
          alert("Student Updated Successfully!");
          window.open('updateform.php?sid=<?php echo $id; ?>','_self');
        </script>
      <?php
    }
  }
?>

<div class="container">
	<form method="post" action="updateform.php" style="margin:auto;top: 0;margin-top: 30px; max-width: 70%; " enctype="multipart/form-data" >
        <h3 align="center">Update Student Information!</h3>
        <br>
        <img align="center" src="../dataimg/<?php echo $data['image']; ?>" style="max-width: 300px;max-height:300px;">
          <div class="form-group">
          <label for="simage">Change Image:</label>
          <input type="file" class="form-control" name="simage" value=" <?php echo $data['image']; ?>" >
        </div>
      
        <div class="form-group">
          <label for="first_name">First name</label>
          <input type="text" class="form-control" name="first_name"  value="<?php echo $data['first_name'] ?>" required>
        </div>
        <div class="form-group">
          <label for="last_name">Last name</label>
          <input type="text" class="form-control" name="last_name" value="<?php echo $data['last_name'] ?>" required>
        </div>
        <div class="form-group">
          <label for="roll_no">Roll no</label>
          <input type="text" class="form-control" name="roll_number"  value="<?php echo $data['roll_number'] ?>" required>
        </div>
        <div class="form-group">
          <label for="email">Email address</label>
          <input type="email" class="form-control" name="email" value="<?php echo $data['email'] ?>" required>
        </div>
        <div class="form-group">
          <label for="standard">Standard</label>
          <input type="number" class="form-control" name="standard"  value="<?php echo $data['standerd'] ?>" required>
        </div> 
    
        <div class="form_group">
        	<input type="hidden" name="sid" value="<?php echo $data['id']; ?>">
        </div>
        <div class="form-group">
        	
        	<button type="submit" name="submit" class="btn btn-warning btn-lg btn-block" style="margin:auto;top:0; ">Submit</button>
        </div>
       
       
    </form>
</div>
</body>
</html>
