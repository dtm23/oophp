<?php 
// Turn off all error reporting
// error_reporting(0);
  include('../dbconnection.php');

  session_start();

  
  if(!isset($_SESSION['uid'])){
    header('location:dashboard.php');
  }
?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">

    <title>SMS</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">

       <style type="text/css">
      body{
        background-color: #EAECEE;
      }
    </style>


  </head>
  <body>
    <nav class="navbar navbar-light bg-light">
      <a class="navbar-brand" href="../index.php">
        <img src="../images/dtmlog.png" width="" height="" alt="">
      </a>
    </nav>