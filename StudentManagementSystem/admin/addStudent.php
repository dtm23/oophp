<?php
  include('adminHead.php');
?>
  
 <div class="container" style="margin-top: 30px;">
  <div class="row">
    <div class="col-8">
      <form method="post" action="addStudent.php" style="margin:auto;top: 0; max-width: 90%;" enctype="multipart/form-data" >
      <h3>Add New Student</h3>
      <br>
      <div class="form-group">
        <label for="first_name">First name</label>
        <input type="text" class="form-control" name="first_name" id="first_name" aria-describedby="" placeholder="" required>
      </div>
      <div class="form-group">
        <label for="last_name">Last name</label>
        <input type="text" class="form-control" name="last_name" id="last_name" aria-describedby="" placeholder="" required>
      </div>
      <div class="form-group">
        <label for="roll_no">Roll no</label>
        <input type="text" class="form-control" name="roll_number" id="roll_no" aria-describedby="" placeholder="" required>
      </div>
      <div class="form-group">
        <label for="email">Email address</label>
        <input type="email" class="form-control" name="email" id="email" aria-describedby="emailHelp" placeholder="" required>
      </div>
      <div class="form-group">
        <label for="standard">Standard</label>
        <input type="number" class="form-control" name="standard" id="standard" aria-describedby="" placeholder="" required>
      </div> 
      <div class="form-group">
        <label for="simage">Images</label>
        <input type="file" class="form-control" name="simage" >
      </div>
     
      <button type="submit" name="submit" class="btn btn-primary">Submit</button>
    </form>
    </div>
    <div class="col-4" style="background-color: ">
      <h5>List of Student's</h5>
    </div>
  </div>

    
 </div>

</body>
</html>

<?php


  if(isset($_POST['submit']))
  {
    $first_name = $_POST['first_name'];
    $last_name = $_POST['last_name'];
    $roll_no = $_POST['roll_number'];
    $email = $_POST['email'];
    $standard = $_POST['standard'];

    $image = $_FILES['simage']['name'];
    $temp_image_location = $_FILES['simage']['tmp_name'];

    move_uploaded_file($temp_image_location, "../dataimg/$image");

    $query = "INSERT INTO `students`(`first_name`, `last_name`, `roll_number`, `email`, `standerd`, `image`) VALUES ('$first_name','$last_name','$roll_no','$email','$standard','$image')";

    $row = mysqli_query($con,$query);

    if($row == true)
    {
      ?>
        <script type="text/javascript">
          alert("Student added Successfully!");
          window.open('dashboard.php');
        </script>
      <?php
    }
  }

?>



