<?php
  include('adminHead.php');
?>

 <div class="container" style="margin-top: 30px;">
  <div class="row">
    
    <div class="col-8">
    <h3 align="center">Remove Student's Profile</h3>
    <br>

    <form method="post" action="deleteStudent.php">
          <div class="form-row">
            <div class="form-group col-md-4">
              <label for="standard">Standard</label>
              <input type="number" class="form-control" name="standard" id="standard" placeholder="" >
            </div>
            <div class="form-group col-md-6">
              <label for="name">Name</label>
              <input type="text" class="form-control" id="last_name" placeholder="" name="last_name" >
            </div>
            <div class="form-group col-md-2">
             <br>
              <button class="btn btn-success" type="submit" name="search" style="margin-top: 8px; width: 100%;">Search</button>
            </div>
          </div>
  </form>
  <hr>
  </div>
  <div class="col-4" style="background-color: "> </div>  
 </div>
 <div class="resultdiv">
   <div class="row">
     <div class="col-12">
         <div style="margin-top: 20px;">
      
          <?php 
          if(isset($_POST['search']))
          {
            
            $standard = $_POST['standard'];
            $last_name = $_POST['last_name'];

            $query = "SELECT * FROM `students` WHERE `standerd` = '$standard' AND `last_name` LIKE '%$last_name%'";

            $run = mysqli_query($con,$query);

            $row = mysqli_num_rows($run);

            if($row<1)
            {
              echo "<tr><td colspan='7'>0 record found!</td></tr>";
            }else
            {
              ?>
              <table class="table" >
                <thead>
                  <tr>
                    <th>s/n</th>
                    <th>Full name</th>
                    <th>Standard</th>
                    <th>Roll no</th>
                    <th>Email</th>
                    <th>Image</th>
                    <th>Edit</th>
                  </tr>
              </thead>
              <tbody>
              <?php
              $count = 0;
              while ($data = mysqli_fetch_assoc($run)) {
                  $count++;
                  $full_name = $data['first_name'].' '.$data['last_name'];
                  ?>
                  
                   <tr>
                    <th scope="row"><?php echo $count; ?></th>
                    <td><?php echo $full_name; ?></td>
                    <td><?php echo $data['standerd']; ?></td>
                    <td><?php echo $data['roll_number']; ?></td>
                    <td><?php echo $data['email']; ?></td>
                    <td><img src="../dataimg/<?php echo $data['image']; ?>" style="max-width: 50px;max-height: 48px;"></td>
                    <td>
                      <button class="btn btn-warning">
                        <a href="deletefrom.php?sid=<?php echo $data['id'] ?>">Delete</a>
                      </button>
                    </td>

                  </tr>
                   <?php
              }?>
              </tbody>
            </table>

            <?php
            }
          }
          ?>

       
      </div>
     </div>
   </div>
 </div>

<!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
</body>
</html>

<?php

if(isset($_SESSION['uid']))
{

  if(isset($_POST['submit']))
  {
    $first_name = $_POST['first_name'];
    $last_name = $_POST['last_name'];
    $roll_no = $_POST['roll_no'];
    $email = $_POST['email'];
    $standard = $_POST['standard'];
    $image = $_FILES['simage']['name'];
    $temp_image_location = $_FILES['simage']['tmp_name'];

    move_uploaded_file($temp_image_location, '../dataimg/$image');

    $query = "INSERT INTO `students`(`first_name`, `last_name`, `roll_number`, `email`, `standerd`, `image`) VALUES ('$first_name','$last_name','$roll_no','$email','$standard','$image')";

    $row = mysqli_query($con,$query);

    if($row == true)
    {
      ?>
        <script type="text/javascript">
          alert("Student added Successfully!");
          window.open('dashboard.php');
        </script>
      <?php
    }
  }
}
?>
